# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Version 0.15.3

- fix guest mail bug

## Version 0.15.2

### Changed

- Support JSON output on utf8 rendering.
- Consistent usage of `click.echo`.

## Version 0.15.1

- fix pdb bug
- add upstream exception if services are not available

## Version 0.15.0

- re-implement mailinglists
- support for .add_members, .del_members, .recertify methods

## Version 0.14.6

- add a flag to allow creating guests which email address already exists

## Version 0.14.5

- fix problem with admingroup for system-generated groups
- allow update description of a persona

## Version 0.14.4

- support for mail address changes for guests

## Version 0.14.3

- fix problems regarding guest and group members

## Version 0.14.2

- fix problem with put request

## Version 0.14.1

- fix pylint issues
- fix service grant success message

## Version 0.14.0

- fix login

## Version 0.13.0

- improve build
- no credentials required for operations that do not need any privileges
- search for guests that do not have any mail address
- make mutations for groups faster
- show warning when hitting CTRL-C while the mutation request was already sent
- drop support for legacy IAM endpoint
- raise exception when account with this email already exists
- implement short list for groups
- show help text for groups and persons when no further option was provided

## Version 0.12.0

- allow searching for groups using email, firstname and lastname of a member
- lookup of persons by their firstname and lastname

## Version 0.11.0

- complete overhaul with many breaking changes
- creating group no longer breaks the system (using new API)

## Version 0.10.0

- add guest list command to get all users of a given host leitzahl

## Version 0.9.0

- add deactivationStartDate, deactivationEndDate for guests

## Version 0.8.0

- allow adding user (personas) to persons (users)

## Version 0.7.2

- allow not to overwrite the initial passwords of a guest user

## Version 0.7.1

- make date conversion/normalisation Windows compatible

## Version 0.7.0

- added initial password for new guests
- added --init-password for users
- fixed password generator

## Version 0.6.3

- fixed notification bug when updating guests

## Version 0.6.2

- added pytz to requirements

## Version 0.6.1

- added python-dateutil to requirements

## Version 0.6.0

- added CLI guest creation/extend support
- added CLI support for group creation
- changed date display format to YYYY-MM-DD
- added unit tests

## Version 0.5.1

- added possibility to access attributes with getitem

## Version 0.5.0

- support of Mailbox service manipulation
- built-in password generator
- password-check
- username-length check

## Version 0.4.0

- various bugfixes
- updated documentation

## Version 0.3.1

- added click and pyyaml to the dependency list

## Version 0.3.0

- added more command line support
- added changing password

## Version 0.2.2

- added support for creating groups

## Version 0.2.1

- improved CLI for better help support and lazy username/password prompt

## Version 0.2.0

- added a small CLI for group management

## Version 0.1.1

- get_group(gidNumber)
- deleting users (personas only) is now possible

## Version 0.1.0

- adding and removing members to and from group now more flexible
- included requests module in requirements.txt

## Version 0.0.4

- bugfix in get_groups

## Version 0.0.3

- added missing doku how to add users

## Version 0.0.2

- group memberships
- fixed timeout problem

## Version 0.0.1

- initial commit
- handling of persons and users
